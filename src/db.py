import sqlite3, os, redis


r = redis.Redis(host='localhost', port=6379, db=0)


def connect_to_db():
    conn = sqlite3.connect("user.db")
    return conn


def check_if_db_exist() -> bool:
    return os.path.exists("user.db")


def create_db() -> None:
    conn = connect_to_db()
    c = conn.cursor()
    c.execute("CREATE TABLE users (user nvarchar, token int)")
    conn.commit()


def exist_user(user: str) -> bool:
    user = user.lower()
    if r.get(user) == None:
        conn = connect_to_db()
        c = conn.cursor()
        c.execute("SELECT * FROM users WHERE user='?'".format(user))
        if not c.fetchone():
            return False
    return True


def create_user(user: str) -> None:
    user = user.lower()
    conn = connect_to_db()
    c = conn.cursor()
    c.execute("INSERT INTO users (user, token) VALUES ('{}', {})".format(user, 0))
    conn.commit()
    r.set(user, 1)


def write_user_token(user: str, token: int):
    user = user.lower()
    conn = connect_to_db()
    c = conn.cursor()
    c.execute("UPDATE users SET token = {} WHERE user = '{}'".format(token, user))
    conn.commit()


def get_user_token(user: str) -> int:
    user = user.lower()
    conn = connect_to_db()
    conn = connect_to_db()
    c = conn.cursor()
    c.execute("SELECT token FROM users WHERE user='{}'".format(user))
    r = c.fetchone()
    return r[0]
