import json


class ConfigError(Exception):
    pass


def test_config() -> dict:
    config = json.load(open("config.json"))
    module = config['module']
    for entry in config["twitch_bot"]:
        if config["twitch_bot"][entry] == "":
            raise ConfigError(
                "You have to fill the entry {} in the config file".format(entry))
    for modul in module:
        if config["module"][modul]:
            for entry in config[modul]:
                if config[modul][entry] == "":
                    raise ConfigError(
                        "You have to fill the entry {} in the config file".format(
                            entry))
    return config


config = test_config() 


def write_config():
    pass


