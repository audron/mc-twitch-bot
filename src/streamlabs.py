#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socketio, asyncio
import util, config, requests


sio = socketio.AsyncClient()


@sio.on("event")
async def twitch_account(data):
    conf = config.config["streamlabs"]
    if data['type'] == 'follow' and conf['follower']:
        user = data["message"][0]["name"]
        util.add_token(user, conf['folower_token'])
    if data['type'] == 'subscription' and conf['subscription']:
        user = data["message"][0]["name"]
        util.add_token(user, conf['subscription_token'])
    if data['type'] == 'donation' and conf['donation']:
        user = data["message"][0]["from"]
        amount = float(data["message"][0]["amount"])
        currency = data["message"][0]["currency"]
        if currency != conf['main_currency']:
            excange = requests.get('https://api.exchangeratesapi.io/latest').json()
            rate = excange['rates'][currency]
            amount = round(currency / rate, 2)
        token = int(round(amount * conf["donation_multi"]))
        util.add_token(user, token)


@sio.event
async def connect():
    print("Connected to streamlabs")


@sio.event
async def connect_error():
    print("The connection failed!")


@sio.event
async def disconnect():
    print("disconnected!")


async def strart_streamlabs():
    token = config.config['streamlabs']['api_token']
    url = "https://sockets.streamlabs.com?token={}".format(token)
    await sio.connect(url)


def thred_streamlebs():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.create_task(strart_streamlabs())
    loop.run_forever()
