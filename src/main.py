import threading
import config, db, bot, streamlabs, watching, util


def main():
    if not db.check_if_db_exist():
        db.create_db()
    util.load_costs()
    if config.config['module']['streamlabs']:
        s = threading.Thread(target=streamlabs.thred_streamlebs)
        s.start()
    if config.config['module']['watchtime']:
        t = threading.Thread(target=watching.reward_user)
        t.start()
    if config.config["twitch_bot"]["default_mc_user"]:
        db.r.set("user", config.config["twitch_bot"]["default_mc_user"])
    bot.bot.run()


if __name__ == "__main__":
    main()
