import random
from twitchio.ext import commands
from mcrcon import MCRcon
import util, db, config


bot_c = config.config['twitch_bot']
mcr = MCRcon(
    bot_c['mc_server_ip'],
    bot_c['mc_server_rcon_pw'],
    port=bot_c['mc_server_rcon_port'])
mcr.connect()

enable = bot_c['enable_on_start'] 
bot = commands.Bot(
    irc_token=bot_c['tmi_token'],
#    client_id=os.environ['CLIENT_ID'],
    nick=bot_c['bot_nick'],
    prefix=bot_c['bot_prefix'],
    initial_channels=[bot_c['channel']])


@bot.event
async def event_message(ctx):
    global enable
    #Some owo here
    if 'owo' == ctx.content.lower():
        await ctx.channel.send('OwO')
    if 'o/' == ctx.content.lower():
        await ctx.channel.send('\\o')
    if 'VoHiYo' in ctx.content:
        await ctx.channel.send('VoHiYo')
    
    #some andmin stuff
    if ctx.author.is_mod:
        if 'a!enable' == ctx.content.lower():
            if db.r.get('user') == None:
                await ctx.channel.send('no user set')
            elif enable == True:
                await ctx.channel.send('the bot ist allready enabled')
            else:
                enable = True
                await ctx.channel.send('the bot is now enabled')
        if 'a!disable' == ctx.content.lower():
            if enable == False:
                await ctx.channel.send('the bot is allready disabled')
            else:
                enable = False
                await ctx.channel.send('the bot is now disabled')
        if 'a!set_user' in ctx.content.lower():
            try:
                user = ctx.content.lower().split('!set_user ')[1]
                db.r.set('user', user)
                await ctx.channel.send('the user was set to {}'.format(user))
            except KeyError:
                await ctx.channel.send('no user found')
        if 'a!toggle' in ctx.content.lower():
            try:
                mode = ctx.content.lower().split("!toggle ")[1]
                if mode == "timer":
                    util.time = not util.time
                    if util.time:
                        en = "enabled"
                    else:
                        en = "disabled"
                    await ctx.channel.send("the timer is now {}".format(en))
                elif mode == "token":
                    util.token = not util.token
                    if util.token:
                        en = "enabled"
                    else:
                        en = "disabled"
                    await ctx.channel.send("the tokens are now {}".format(en))
                else:
                    ctx.channel.send("mode not found")
            except KeyError:
                ctx.channel.send("no mode found")
        if 'a!give_token' in ctx.content.lower():
            try:
                user_token = ctx.content.lower().split("give_token ")[1]
                user = user_token.split(' ')[0]
                try:
                    if not db.exist_user(user):
                        db.create_user(user)
                    token = int(user_token.split(" ")[1])
                    old_token = db.get_user_token(user)
                    db.write_user_token(user, old_token + token)
                    await ctx.channel.send(
                        "{} has now {} UwU tokens".format(user, old_token + token))
                except KeyError:
                    await ctx.channel.send("token not found")
            except KeyError:
                await ctx.channel.send("user not found")
        if 'a!death_count' in ctx.content.lower():
            resp = mcr.command('scoreboard objectives add Deaths deathCount')
            print(resp)
            resp = mcr.command('scoreboard objectives setdisplay list Deaths')
            print(resp)

    if enable:
        await bot.handle_commands(ctx)


@bot.command(name='spawn')
async def spawn(ctx):
    try:
        enty_emount = ctx.content.lower().split('spawn ')[1]
        if " " in enty_emount:
            enty = enty_emount.split(" ")[0]
            emount = int(enty_emount.split(" ")[1])
        else:
            enty = enty_emount
            emount = 1
        max_emount = config.config['twitch_bot']['spawn_limit']
        if emount > max_emount:
            emount = max_emount 
        if "dragon" in enty or "wither" == enty:
            emount = 1
        cost = util.get_cost("spawn", enty)
        cost = cost * emount
        if util.check_user_allowed(ctx.author.name, cost, "spawn"):
            i = 0
            user = db.r.get("user").decode("utf-8")
            while i < emount:
                resp = mcr.command(
                    '/execute as {} at {} run summon {} ~ ~ ~'.format(user, user, enty))
                i += 1 
            if 'Unknown entity' in resp:
                await ctx.send('mob "{}" not found ┐(‘～`；)┌'.format(enty))
            else:
                await ctx.send('spawned {} {}'.format(emount, enty))
                msg = util.reset_timer_usesr_token(ctx.author.name, cost, "spawn")
                if msg:
                    await ctx.send(msg)
        else:
            await ctx.send(
                util.error_time_or_token(ctx.author.name, cost, "spawn"))
    except IndexError:
        await ctx.send(
            '{}!spawn (mob) (emount | default 1, max 10)'.format(
                util.error_field("mob")))


@bot.command(name='give')
async def give_item(ctx):
    try:
        item_emount = ctx.content.lower().split("give ")[1]
        if " " in item_emount:
            item = item_emount.split(" ")[0]
            emount = int(item_emount.split(" ")[1])
        else:
            item = item_emount
            emount = 1
        cost = util.get_cost("give", item)
        cost = int(round(cost * emount, 0))
        if util.check_user_allowed(ctx.author.name, cost, "give"):
            user = db.r.get("user").decode("utf-8")
            if emount > 1:
                emount = " {}".format(emount)
            else:
                emount = ""
            resp = mcr.command('/give {} {}{}'.format(user, item, emount))
            print(resp)
            if 'Unknown' in resp:
                await ctx.send('item "{}" not found (◕︵◕)'.format(item))
            else:
                resp = mcr.command(
                    'say you got {} {} from {}'.format(
                        emount, item, ctx.author.name))
                await ctx.send("gived {} {}".format(emount, item))
                msg = util.reset_timer_usesr_token(ctx.author.name, cost, "give")
                if msg:
                    await ctx.send(msg)
        else:
            await ctx.send(
                util.error_time_or_token(ctx.author.name, cost * emount, "give"))
    except IndexError:
        await ctx.send(
            '{}!give (item) (emount | default 1)'.format(util.error_field("item")))


@bot.command(name='effect')
async def give_effect(ctx):
    try:
        effect_time_amplifier = ctx.content.lower().split("effect ")[1]
        if " " in effect_time_amplifier:
            effect = effect_time_amplifier.split(" ")[0]
            time = int(effect_time_amplifier.split(" ")[1])
            try:
                amplifier = int(effect_time_amplifier.split(" ")[2])
            except KeyError:
                amplifier = 1
        else:
            effect = effect_time_amplifier
            time = 10
            amplifier = 1
        if time > 60:
            time = 60
        if amplifier > 60:
            amplifier = 60
        cost = util.get_cost("effect", effect)
        cost = int(round(cost * (time / 10) * amplifier, 0))
        if util.check_user_allowed(ctx.author.name, cost, "effect"):
            if amplifier > 1:
                amplifier = " " + amplifier
            else:
                amplifier = ""
            user = db.r.get("user").decode("utf-8")
            resp = mcr.command(
                '/effect give {} {} {}{}'.format(user, effect, time, amplifier))
            print(resp)
            if 'Unknown effect' in resp:
                await ctx.send('effect "{}" not found o(╥﹏╥)o'.format(effect))
            else:
                await ctx.send("given effect {} for {} sec".format(effect, time))
                msg = util.reset_timer_usesr_token(ctx.author.name, cost, "effect")
                if msg:
                    await ctx.send(msg)
        else:
            await ctx.send(util.error_time_or_token(ctx.author.name, cost, "effect"))
    except IndexError:
        await ctx.send(
            '''{}!effect (effect) (time | default 10, max 60) 
            (amplifier | default 1, max 60)'''.format(util.error_field("effect")))


@bot.command(name='teleport')
async def teleport(ctx):
    cost = util.get_cost("tp", "default")
    if util.check_user_allowed(ctx.author.name, cost, "tp"):
        x = random.randint(-10000, 10000)
        z = random.randint(-10000, 10000)
        y = 110
        user = db.r.get('user').decode('utf-8')
        resp = mcr.command('tp {} {} {} {}'.format(user, x, y, z))
        print(resp)
        resp = mcr.command(
            'effect give {} minecraft:resistance 4 255'.format(user))
        print(resp)
        await ctx.send('teleported to {} {}'.format(x, z))
        msg = util.reset_timer_usesr_token(ctx.author.name, cost, "tp")
        if msg:
            await ctx.send(msg)
    else:
        await ctx.send(util.error_time_or_token(ctx.author.name, cost, "tp"))


@bot.command(name='kill')
async def kill(ctx):
    cost = util.get_cost("kill", "default")
    if util.check_user_allowed(ctx.author.name, cost, "kill"):
        user = db.r.get('user').decode('utf-8')
        resp = mcr.command('kill {}'.format(user))
        print(resp)
        await ctx.send('killing (╬ ಠ益ಠ)')
        msg = util.reset_timer_usesr_token(ctx.author.name, cost, "kill")
        if msg:
            await ctx.send(msg)
    else:
        await ctx.send(util.error_time_or_token(ctx.author.name, cost, "kill"))


@bot.command(name='mlg')
async def mlg(ctx):
    cost = util.get_cost("mlg", "default")
    if util.check_user_allowed(ctx.author.name, cost, "mlg"):
        user = db.r.get("user").decode("utf-8")
        await ctx.send("MLG ( •_•)>⌐■-■  (⌐■_■)")
        resp = mcr.command('/execute as {0} at {0} run tp {0} ~ 300 ~'.format(user))
        print(resp)
        command1 = '/execute as {0} at {0} run summon minecraft:item ~ 270 ~ '.format(user)  
        command2 = '{Item:{id:"minecraft:water_bucket",Count:1b}}'
        resp = mcr.command(command1 + command2)
        print(resp)
        msg = util.reset_timer_usesr_token(ctx.author.name, cost, "mlg")
        if msg:
            await ctx.send(msg)
    else:
        await ctx.send(util.error_time_or_token(ctx.author.name, cost, "mlg"))


@bot.command(name='goal')
async def goal(ctx):
    pass


@bot.command(name='token')
async def list_token(ctx):
    if not db.exist_user(ctx.author.name):
        db.create_user(ctx.author.name)
    token = db.get_user_token(ctx.author.name)
    if token != 0:
        await ctx.send("you have {} UwU Tokens".format(token))
    else:
        await ctx.send("you dont have any tokens \n (╥﹏╥)")


async def start_bot() -> None:
    bot.run()
