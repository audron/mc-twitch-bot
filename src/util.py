import random, json
import db, config
from datetime import datetime


over_all_cost = {}
#config = {}
#time = False
#token = True 


def random_time(first: int, last: int) -> (float, int):
    now = datetime.timestamp(datetime.now())
    rand = random.randint(first * 60, last * 60)
    return (now + rand, rand)


def is_time_pass(mode: str) -> bool:
    time = db.r.get(mode)
    if time != None:
        time = float(time.decode("utf-8"))
        now = datetime.timestamp(datetime.now())
        return now > time
    return True


def print_time(time: float) -> str:
    now = datetime.timestamp(datetime.now())
    time = time - now
    m = time / 60
    s = round(time % 60, 0)
    _m = ""
    if m >= 1:
        _m += '{} min'.format(str(m).split(".")[0])
    if s > 0:
        if _m != "":
            _m += " and "
        _m += '{} sec'.format(s)
    _m += " left"
    return _m


def remove_token(user: str, emount: int) -> None:
    token = db.get_user_token(user)
    db.write_user_token(user, token - emount)


def add_token(user: str, emount: int) -> None:
    if not db.exist_user(user):
        db.create_user(user)
    token = db.get_user_token(user)
    db.write_user_token(user, token + emount)


def reset_timer_usesr_token(user: str, cost: int, mode: str):
    time = config.config["mode"]["time"]
    token = config.config["mode"]["token"]
    msg = None
    if token:
        token = db.get_user_token(user)
        print(
            "reset token: {} | cost: {} | newToken: {}".format(
                token, cost, token - cost))
        db.write_user_token(user, token - cost)
    if time:
        time, rand = random_time(config.config["time"][mode])
        _min = rand / 60
        _sec = rand % 60
        if _min >= 1:
            _min = "{} min".format(str(_min).split(".")[0])
        else:
            _min = ""
        if _sec > 0:
            if _min != "":
                _min += " and "
            _min += "{} sec".format(int(round(_sec, 0)))
        msg = "the timer has ben set to {}".format(_min)
        db.r.set(mode, time)
    return msg


def check_user_allowed(user: str, cost: int, mode: str) -> bool:
    time = config.config["mode"]["time"]
    token = config.config["mode"]["token"]
    if time or token:
        if time:
            return is_time_pass(mode) 
        elif token:
            return has_enouth_token(user, cost)
        else:
            return is_time_pass(mode) and has_enouth_token(user, cost)
    return True


def has_enouth_token(user: str, cost: int) -> bool:
    if db.exist_user(user):
        token = db.get_user_token(user)
        time = datetime.now()
        print("time: {} | token: {} | cost: {}".format(time, token, cost))
        return token >= cost
    else:
        db.create_user(user)
        return False


def error_time_or_token(user: str, cost: int, mode: str) -> str:
    time = config.config["mode"]["time"]
    token = config.config["mode"]["token"]
    if time:
        _time = float(db.r.get(mode).decode("utf-8"))
        msg = "the cooldown time has not passt. {}".format(print_time(_time))
    elif token:
        token = db.get_user_token(user)
        msg = "you dont have enough tokens. costs: {}, your tokens: {}.".format(
            cost, token)
    else:
        _time = float(db.r.get(mode).decode("utf-8"))
        token = db.get_user_token(user)
        msg = '''the cooldown time has not passt or you dont have enough tokens.
        {}. costs: {}, your tokens: {}.'''.format(print_time(_time, cost, token))
    return msg


def error_field(field: str) -> str:
    return "field {} not found. How to use -> ".format(field)


#def load_config():
#    global config
#    global time
#    global token
#    config = json.load(open("config.json", 'r'))
#    time = config["mode"]["time"]
#    token = config["mode"]["token"]


def load_costs():
    global over_all_cost
    over_all_cost = json.load(open("cost.json", "r"))


def get_cost(mode: str, field: str) -> int:
    global over_all_cost
    try:
        _cost = over_all_cost[mode][field]
        return _cost
    except KeyError:
        return over_all_cost[mode]["default"]
